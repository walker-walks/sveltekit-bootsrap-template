import preprocess from 'svelte-preprocess';

/** @type {import('@sveltejs/kit').Config} */
const config = {
  // Consult https://github.com/sveltejs/svelte-preprocess
  // for more information about preprocessors
  preprocess: preprocess({
    scss: {
      includePaths: ['src', 'src/lib', 'src/lib/style'],
      prependData: `@import 'src/lib/style/variables';`,
      outputStyle: 'compressed',
    },
    preserve: ['ld+json'],
  }),

  kit: {
    // hydrate the <div id="svelte"> element in src/app.html
    target: '#svelte',
  },
};

export default config;
