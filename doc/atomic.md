ref: https://uxdesign.cc/grouping-components-in-atomic-design-systems-4d6e2095ea45

TODO

## ATOMS

Typographic styles
Color swatches
Icons
Radio buttons
Checkboxes
Sliders
Toggles
Profile pictures placeholders
Product pictures placeholders
Buttons
Links
Input fields (without labels)
Tabs
Pills
Badges
Tags
Tooltips

## MOLECULES

Dropdown menus
Radio buttons inside regular buttons
Dropdown buttons
Date pickers
Search components
Blockquotes
Breadcrumbs
Card components
Collapsible group items
Input fields (with labels)
I̶n̶p̶u̶t̶ ̶g̶r̶o̶u̶p̶s̶ \* removed as per update [1]
Media uploaders
Loading components
Notifications
Pagination
Media objects
Informative pop-ups e.g., error messages
Boolean modals e.g., This site uses cookies: accept/cancel
Tiles

## ORGANISMS

Navigation bars
Tab bars
Video players
Music players
Media grids
Progress indicator
Tables
Carousels
Forms

## Templates/Layout

Blog
Login
auth

## Page
