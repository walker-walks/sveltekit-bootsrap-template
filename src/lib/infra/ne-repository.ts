import { loadRepository } from './nne.conf';

export const neRepository = (name: string) => ({
  // find
  find: async (filter?: any) => {
    const db = await loadRepository(name as any);
    return new Promise((resolve, reject) => {
      db.find(filter, function (err, docs) {
        if (err) {
          reject(err);
          return;
        }
        resolve(docs);
      });
    });
  },
  // insert
  insert: async (value) => {
    const db = await loadRepository(name as any);
    return new Promise((resolve, reject) => {
      db.insert(value, function (err) {
        if (err) {
          reject(err);
          return;
        }
        resolve('success');
      });
    });
  },
  // update
  update: async (needle, value, options) => {
    const db = await loadRepository(name as any);
    return new Promise((resolve, reject) => {
      db.update(needle, value, options, function (err) {
        if (err) {
          reject(err);
          return;
        }
        resolve('success');
      });
    });
  },
});
