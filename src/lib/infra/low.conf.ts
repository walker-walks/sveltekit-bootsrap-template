// import { Low, JSONFile } from 'lowdb';
// import { Low } from 'lowdb/lib/Low';
// import { JSONFile } from 'lowdb/lib/adapters/JSONFile';
import { join, dirname } from 'path';
import { Low, JSONFile } from 'lowdb';

const __dirname = dirname(`${process.cwd()}`);
const file = join(__dirname, 'datas/dados-gov.low.json');

// const file = `${process.cwd()}/datas/dados-gov.low.json`;
const adapter = new JSONFile(file);

let data: Low<any>;

export async function lowData<T>(path: string) {
  if (data === undefined) {
    const db = new Low<any>(adapter);
    db.data = db.data || {
      tag: [],
      group: [],
      package: [],
    };
    data = db.data;
    await db.read();
  }

  return data[path];
}
