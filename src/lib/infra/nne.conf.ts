import Datastore from 'nedb';

const tag = new Datastore({
  filename: `${process.cwd()}/datas/dados-gov.tag.json`,
});
const group = new Datastore({
  filename: `${process.cwd()}/datas/dados-gov.group.json`,
});
const dbMap = {
  tag,
  group,
};

const db: {
  group: Datastore<any>;
  tag: Datastore<any>;
} = {
  group: undefined,
  tag: undefined,
};

export function loadRepository<T>(key: keyof typeof db): Promise<Datastore<T>> {
  if (db[key] === undefined) {
    // You need to load each database (here we do it asynchronously)
    return new Promise((resolve, reject) => {
      dbMap[key].loadDatabase((err) => {
        console.log('loadDatabase', err);
        if (err) {
          reject(err);
          return;
        }
        db[key] = group;
        resolve(db[key]);
      });
    });
  }

  return Promise.resolve(db[key]);
}
