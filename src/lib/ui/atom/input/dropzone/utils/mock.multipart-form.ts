import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const mockMultipart = new MockAdapter(axios);

// this mocks a request which is always at 40% progress
const sleep = (value) => new Promise((resolve) => setTimeout(resolve, value));
mockMultipart.onPost('/upload').reply(async (config) => {
  const total = 1024; // mocked file size
  const steps = [...new Array(10).keys()].map((v) => v + 1 / 10);
  for (const currentProgress of steps) {
    await sleep(300);
    if (config.onUploadProgress) {
      config.onUploadProgress({ loaded: total * currentProgress, total });
    }
  }
  return [200, null];
});
