const host = 'https://dados.gov.br/api/3/';

export const dadosGovApi = {
  action: {
    group: {
      meta: `${host}action/group_list`,
      index: `${host}action/group_show`,
    },
    tag: {
      meta: `${host}action/tag_list`,
      index: `${host}action/tag_show`,
    },
    package: {
      meta: `${host}action/package_list`,
      index: `${host}action/package_show`,
      updates: `${host}action/recently_changed_packages_activity_list`,
      search: (q: string) => `${host}action/package_search?q=${q}`,
    },
  },
};
