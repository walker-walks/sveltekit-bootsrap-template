Data & Community
stackoverflow:

- https://startbootstrap.com/snippets/portfolio-item
- https://keystonejs.com/docs/guides/document-fields#related-resources
- https://github.com/
- https://ja.stackoverflow.com/
- https://about.gitlab.com/
- https://www.slideshare.net/
- https://www.kaggle.com/#
- https://freefrontend.com/bootstrap-code-examples/
  coloring

indie

- https://www.getapp.com/

Rivals:

- https://www.getapp.com/p/sem/document-management-software?t=Best%20Document%20Management%20Software&camp=adw_search&utm_content=g&utm_source=ps-google&utm_campaign=COM_MISC_Desktop_BE-Document_Management&utm_medium=cpc&account_campaign_id=1541494256&account_adgroup_id=70352323428&ad_id=367731240448&utm_term=slideshare%20alternative&matchtype=e&gclid=Cj0KCQjwnJaKBhDgARIsAHmvz6fG62DGspO5XllL_UOK5z7D3mCHd8QsIuTl1Nf4u36PzWf3EwOGAO4aAvIHEALw_wcB

data sites:

- https://dados.gov.br/
- https://ok.org.br/projetos/
- https://www.opengovpartnership.org/members/brazil/commitments/BR0018/
- https://fiquemsabendo.com.br/?gclid=CjwKCAjw-ZCKBhBkEiwAM4qfF8Z-N_bQkmllXLpTYJhFfACdMOfhnknyJmw1zYBKrUWInbzsu0LvbRoCtHgQAvD_BwE
- https://www.opendata.com.br/
- https://opendatasus.saude.gov.br/dataset

templates:

- https://startbootstrap.com/
- https://getbootstrap.com/docs/4.4/examples/
- https://themes.getbootstrap.com/
- https://colorlib.com/wp/free-bootstrap-blog-templates/
- https://themeforest.net/category/site-templates?gclid=Cj0KCQjws4aKBhDPARIsAIWH0JX7TgdV8Eof67CwM4tFP7V4qnDCE344td8EMEgVrQNAGKPgM0hREM8aAsQPEALw_wcB&referrer=search&sort=sales&tags=bootstrap&utf8=%E2%9C%93&view=grid
- https://www.free-css.com/template-categories/bootstrap
  css:
- https://blog.hubspot.com/website/free-bootstrap-based-wordpress-themes
- https://speckyboy.com/free-bootstrap-framework-templates/
- https://bootswatch.com/
- https://colorlib.com/wp/
- https://themefisher.com/free-bootstrap-templates/
- https://mobirise.com/bootstrap-template/
